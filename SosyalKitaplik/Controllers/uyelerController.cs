﻿using SosyalKitaplik.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SosyalKitaplik.Controllers
{
    public class uyelerController : Controller
    {

        private SAHAFEntities db = new SAHAFEntities();
        //Models.uyeler dbDeneme = new Models.uyeler();
        //Models.adresler dbDeneme1 = new Models.adresler();
        //Models.odeme dbDenem2 = new Models.odeme();
        // GET: uyeler
        public ActionResult Giris()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Giris(uyeler model)
        {
            var kullanici = db.uyeler.FirstOrDefault(x => x.Uye_Adi == model.Uye_Adi && x.sifre == model.sifre);
            if(kullanici != null)
            {
                Session["Uye_Adi"] = kullanici;
                return RedirectToAction("Index", "Admin");
            }
            //ViewBag.HATA("Kullanıcı Adı veya Şifre Yanlış");
            return View();
        }

        public ActionResult KayıtOl()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KayıtOl(uyeler uyelerM, adresler adreslerM, odeme odemeM)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            Models.uyeler dbDeneme = new Models.uyeler();
            Models.adresler adres = new Models.adresler();

            var basTar = DateTime.Now.ToString("MM / gg / yyyy ss: dd tt");
            ViewData["basTar"] = basTar;
        
            dbDeneme.Uye_Adi = uyelerM.Uye_Adi;
            dbDeneme.Uye_Soyadi = uyelerM.Uye_Soyadi;
            dbDeneme.Mail = uyelerM.Mail;
            dbDeneme.Baslangic_Tarihi = uyelerM.Baslangic_Tarihi;
           
            dbDeneme.Bitis_Tarihi = uyelerM.Bitis_Tarihi;
            dbDeneme.Uye_Tel = uyelerM.Uye_Tel;
            dbDeneme.sifre = uyelerM.sifre;

            adres.Kapı_No = adreslerM.Kapı_No;
            adres.Sokak = adreslerM.Sokak;
            adres.Ilce = adreslerM.Ilce;
            adres.Il = adreslerM.Il;
            adres.Mahalle = adreslerM.Mahalle;

            db.adresler.Add(adres);
            db.uyeler.Add(dbDeneme);

           
            try
            {
             
                db.SaveChanges();

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage
                            );
                        raise = new InvalidCastException(message, raise);
                    }
                }
                throw raise;
            }
           
            return View();
        }
    }
}